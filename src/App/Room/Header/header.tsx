import { observer } from "mobx-react";
import { FunctionComponent } from "react";
import { chatStore } from "../../../Store/chatStore";
import { Title } from "../../../AKit/Title/title";
import { ButtonIcon } from "../../../AKit/Button/ButtonIcon/buttonIcon";
import { ExitIcon } from "../../../Icon/exitIcon";
import styles from "./header.module.scss";

interface HeaderProps {}

const onClick = () => {
  chatStore.exitChat();
};

export const Header: FunctionComponent<HeaderProps> = observer(() => {
  const roomName = chatStore.roomName;

  return (
    <div className={styles.container}>
      <Title lavel={4} children={roomName} color="light" />
      <ButtonIcon onClick={onClick} children={<ExitIcon />} color="light" />
    </div>
  );
});
