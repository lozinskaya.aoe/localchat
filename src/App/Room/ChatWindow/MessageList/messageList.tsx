import { observer } from "mobx-react";
import { FunctionComponent, useRef } from "react";
import { Message } from "./Message/message";
import { chatStore } from "../../../../Store/chatStore";
import { useScrollToLastMessage } from "../../../../Hook/useScrollToLastMessage";
import { useSyncWithBatabase } from "../../../../Hook/useSyncWithBatabase";
import styles from "./messageList.module.scss";

interface MessageListProps {}

export const MassageList: FunctionComponent<MessageListProps> = observer(() => {
  const messages = chatStore.messangeList;
  const currentUserName = chatStore.user?.name;
  const ref = useRef<HTMLDivElement>(null);

  useScrollToLastMessage(ref, messages);
  useSyncWithBatabase();

  return (
    <div className={styles.container} ref={ref}>
      {messages.map((message) => (
        <Message
          key={message.id}
          message={message}
          isUserMessage={message.userName === currentUserName}
        />
      ))}
    </div>
  );
});
