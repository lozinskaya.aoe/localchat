import { FunctionComponent } from "react";
import { Bubble } from "./Bubble/bubble";
import { IMessage } from "../../../../../Model/message";
import { AvatarOneIcon } from "../../../../../Icon/Avatar/avatarOneIcon";
import styles from "./message.module.scss";

interface MessageProps {
  message: IMessage;
  isUserMessage: boolean;
}

export const Message: FunctionComponent<MessageProps> = ({
  message,
  isUserMessage,
}) => {
  const className = ` ${styles.container} ${
    isUserMessage ? styles.right : styles.left
  }`;

  return (
    <div className={className} id={`message-${message.id}`}>
      <AvatarOneIcon />
      <Bubble message={message} />
    </div>
  );
};
