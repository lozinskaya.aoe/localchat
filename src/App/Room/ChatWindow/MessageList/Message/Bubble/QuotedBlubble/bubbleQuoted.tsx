import { FunctionComponent } from "react";
import { ReplayMessage } from "../../../../Components/ReplayMessage/replayMessage";
import { IQuotedMessage } from "../../../../../../../Model/message";
import { scrollToQuotedElement } from "../../../../../../../Utils/scrollToQuotedElenemt";
import { chatStore } from "../../../../../../../Store/chatStore";
import styles from "./bubbleQuoted.module.scss";

interface QuotedBubbleProps {
  quotedMessage: IQuotedMessage;
}

export const QuotedBubble: FunctionComponent<QuotedBubbleProps> = ({
  quotedMessage,
}) => {
  const onQuotedMessageClick = (): void => {
    const elementById = `message-${quotedMessage.id}`;
    scrollToQuotedElement(elementById, styles.select);
  };

  const message = chatStore.getMessgeQuoted(quotedMessage.id);
  return (
    <div className={styles.container} onClick={onQuotedMessageClick}>
      {message ? <ReplayMessage message={message} /> : "сообщение удалено"}
    </div>
  );
};
