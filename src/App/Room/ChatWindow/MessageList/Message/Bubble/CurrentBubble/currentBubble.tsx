import { FunctionComponent } from "react";

import styles from "./currentBubble.module.scss";
import { IMessage } from "../../../../../../../Model/message";
import { Text } from "../../../../../../../AKit/Text/text";
import { Title } from "../../../../../../../AKit/Title/title";
import { DateFormatter } from "../../../../../../../Utils/dateFormatter";

interface CurrentBubbleProps {
  message: IMessage;
}

export const CurrentBubble: FunctionComponent<CurrentBubbleProps> = ({
  message,
}) => {
  const imageContent = message.content.image ? (
    <img src={message.content.image.imagePreviewUrl} className={styles.image} />
  ) : (
    <Text children={message.content.text} />
  );

  return (
    <div className={styles.container}>
      <Title children={message.userName} lavel={4} />
      {imageContent}
      <Text
        children={DateFormatter.fromString(message.date)}
        color="grey"
        textAlign="end"
      />
    </div>
  );
};
