import { FunctionComponent } from "react";
import { CurrentBubble } from "./CurrentBubble/currentBubble";
import { QuotedBubble } from "./QuotedBlubble/bubbleQuoted";
import { IMessage } from "../../../../../../Model/message";
import { chatStore } from "../../../../../../Store/chatStore";
import styles from "./bubble.module.scss";

interface BubbleProps {
  message: IMessage;
}

export const Bubble: FunctionComponent<BubbleProps> = ({ message }) => {
  const onBubbleClick = (): void => {
    chatStore.setCurrentMessageContent("quotedMessage", message);
  };

  return (
    <div className={styles.container} onDoubleClick={onBubbleClick}>
      {message.content.quotedMessage !== null && (
        <QuotedBubble quotedMessage={message.content.quotedMessage} />
      )}

      <CurrentBubble message={message} />
    </div>
  );
};
