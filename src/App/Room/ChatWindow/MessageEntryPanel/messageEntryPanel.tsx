import { FunctionComponent } from "react";
import { observer } from "mobx-react";
import { AddImageButton } from "./AddImageButton/addImageButton";
import { QuotedMessage } from "./QuotedMessage/quotedMessage";
import { chatStore } from "../../../../Store/chatStore";
import { usePressEnter } from "../../../../Hook/usePressEnter";
import { InputWithIcon } from "../../../../AKit/Input/InputWithIcon/inputWithIcon";
import { ButtonIcon } from "../../../../AKit/Button/ButtonIcon/buttonIcon";
import { AirplaneIcon } from "../../../../Icon/airplaneIcon";
import styles from "./messageEntryPanel.module.scss";

interface MessageEntryPanelProps {}
    
const onMessageChange = (text: string) => {
      chatStore.setCurrentMessageContent("text", text);
    };

    const onMessageSend = () => {
      chatStore.sendMessange();
    };

export const MessageEntryPanel: FunctionComponent<MessageEntryPanelProps> =
  observer(() => {
    const text = chatStore.currentMessageContent.text;
    const quotedMessage = chatStore.currentMessageContent.quotedMessage;
    const hasNotQuotedMessageOrText = quotedMessage === null && text === "";
    const hasText = text !== "";
    const hasQuotedMessage = quotedMessage !== null;

    usePressEnter(onMessageSend);

    return (
      <div className={styles.container}>
        {hasQuotedMessage && <QuotedMessage quotedMessage={quotedMessage} />}

        <InputWithIcon
          value={text}
          startContent={hasNotQuotedMessageOrText && <AddImageButton />}
          endContent={
            hasText && (
              <ButtonIcon children={<AirplaneIcon />} onClick={onMessageSend} />
            )
          }
          onChange={onMessageChange}
        />
      </div>
    );
  });
