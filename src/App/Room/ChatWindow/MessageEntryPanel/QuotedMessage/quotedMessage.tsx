import { FunctionComponent } from "react";
import { IQuotedMessage } from "../../../../../Model/message";
import { chatStore } from "../../../../../Store/chatStore";
import { scrollToQuotedElement } from "../../../../../Utils/scrollToQuotedElenemt";
import { ReplayIcon } from "../../../../../Icon/replayIcon";
import { Icon } from "../../../../../AKit/Icon/icon";
import { ReplayMessage } from "../../Components/ReplayMessage/replayMessage";
import { ButtonIcon } from "../../../../../AKit/Button/ButtonIcon/buttonIcon";
import { CloseIcon } from "../../../../../Icon/closeIcon";
import styles from "./quotedMessage.module.scss";

interface QuotedMessageProps {
  quotedMessage: IQuotedMessage;
}

export const QuotedMessage: FunctionComponent<QuotedMessageProps> = ({
  quotedMessage,
}) => {
  const message = chatStore.getMessgeQuoted(quotedMessage.id);

  const onQuotedMessageClick = () => {
    const elementById = `message-${quotedMessage.id}`;
    scrollToQuotedElement(elementById, styles.select);
  };

  if (!message) {
    return null;
  }

  const onQuotedMessageClose = () => {
    chatStore.setCurrentMessageContent("quotedMessage", null);
  };

  return (
    <div className={styles.container}>
      <div className={styles.content} onClick={onQuotedMessageClick}>
        <Icon children={<ReplayIcon className={styles.icon} />} />
        <ReplayMessage message={message} />
      </div>

      <ButtonIcon children={<CloseIcon />} onClick={onQuotedMessageClose} />
    </div>
  );
};
