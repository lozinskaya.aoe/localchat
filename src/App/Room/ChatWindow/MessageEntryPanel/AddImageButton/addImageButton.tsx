import { FunctionComponent, useState } from "react";
import { ImageWorkingPanel } from "../../Components/ImageWorkingPanel/imageWorkingPanel";
import { ButtonIcon } from "../../../../../AKit/Button/ButtonIcon/buttonIcon";
import { ButtonLoadend } from "../../../../../AKit/Loadend/loadend";
import { ImageIcon } from "../../../../../Icon/imageIcon";

interface AddImageButtonProps {}

export const AddImageButton: FunctionComponent<AddImageButtonProps> = ({}) => {
  const [file, setFile] = useState<FileList | null>(null);

  const onButtonClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFile(event.target.files);
  };

  return (
    <>
      <ButtonIcon>
        <ButtonLoadend
          onClick={onButtonClick}
          children={<ImageIcon />}
          key={`loaden-${file?.length ?? 0}`}
        />
      </ButtonIcon>

      {file !== null && (
        <ImageWorkingPanel file={file} emptyFile={() => setFile(null)} />
      )}
    </>
  );
};
