import { FunctionComponent } from "react";
import { MassageList } from "./MessageList/messageList";
import { MessageEntryPanel } from "./MessageEntryPanel/messageEntryPanel";
import styles from "./chatWindow.module.scss";

interface ChatWindowProps {}

export const ChatWindow: FunctionComponent<ChatWindowProps> = ({}) => {
  return (
    <div className={styles.container}>
      <MassageList />
      <MessageEntryPanel/>
    </div>
  );
};
