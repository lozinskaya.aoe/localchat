import { FunctionComponent, useEffect } from "react";
import { observer } from "mobx-react";
import { chatStore } from "../../../../../Store/chatStore";
import { Modal } from "../../../../../AKit/Modal/modal";
import { Space } from "../../../../../AKit/Space/space";
import { Title } from "../../../../../AKit/Title/title";
import { Button } from "../../../../../AKit/Button/button";
import styles from "./imageWorkingPanel.module.scss";

interface ImageWorkingPanelProps {
  file: FileList;
  emptyFile: () => void;
}

const updateImageMessgeContent = (file: FileList): void => {
  const reader = new FileReader();
  reader.onload = () => {
    const img = {
      file: file[0],
      imagePreviewUrl: reader.result as string,
    };
    chatStore.setCurrentMessageContent("image", img);
  };

  reader.readAsDataURL(file[0]);
};

export const ImageWorkingPanel: FunctionComponent<ImageWorkingPanelProps> =
  observer(({ file, emptyFile }) => {
    const image = chatStore.currentMessageContent.image;

    useEffect(() => {
      if (file) {
        updateImageMessgeContent(file);
      }
    }, []);

    const onCancel = (): void => {
      chatStore.setCurrentMessageContent("image", null);
      emptyFile();
    };

    const onImageSend = () => {
      if (image) {
        chatStore.sendMessange();
        emptyFile();
      }
    };

    return (
      <Modal>
        <Space gap={20} hasFillSpace={true}>
          <Title children="Отправить изображение" className={styles.full} />

          <div className={styles.container}>
            <img src={image?.imagePreviewUrl} className={styles.full} />
          </div>
          <Space direction="horizontal" align="right" hasFillSpace={true}>
            <Button text="Отменить" onClick={onCancel} />
            <Button text="Отправить" onClick={onImageSend} />
          </Space>
        </Space>
      </Modal>
    );
  });
