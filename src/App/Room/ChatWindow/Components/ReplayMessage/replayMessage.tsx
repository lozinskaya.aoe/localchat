import { FunctionComponent } from "react";
import { IMessage } from "../../../../../Model/message";
import { Space } from "../../../../../AKit/Space/space";
import { Title } from "../../../../../AKit/Title/title";
import { Text } from "../../../../../AKit/Text/text";
import styles from "./replayMessage.module.scss";

interface ReplayMessageProps {
  message: IMessage;
}

export const ReplayMessage: FunctionComponent<ReplayMessageProps> = ({
  message,
}) => {
  const isImageContent = message.content.image !== null;

  if (isImageContent) {
    return (
      <Space direction="horizontal" gap={10} align="left">
        <img
          src={message.content.image?.imagePreviewUrl}
          className={styles.image}
        />
        <Space gap={5}>
          <Title lavel={4} className={styles.title}>
            {message.userName}
          </Title>
          <Text>Фото</Text>
        </Space>
      </Space>
    );
  }

  return (
    <Space gap={5} align="left">
      <Title lavel={4} className={styles.title}>
        {message.userName}
      </Title>
      <Text>{message.content.text}</Text>
    </Space>
  );
};
