import { FunctionComponent } from "react";
import { Space } from "../../AKit/Space/space";
import { Header } from "./Header/header";
import { ChatWindow } from "./ChatWindow/chatWindow";


interface RoomProps {}

export const Room: FunctionComponent<RoomProps> = ({}) => {
  return (
    <Space hasFillSpace={true}>
      <Header />
      <ChatWindow />
    </Space>
  );
};
