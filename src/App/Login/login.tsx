import { FunctionComponent, useCallback, useEffect, useState } from "react";
import { observer } from "mobx-react";
import { chatStore } from "../../Store/chatStore";
import { Text } from "../../AKit/Text/text";
import { Space } from "../../AKit/Space/space";
import { Title } from "../../AKit/Title/title";
import { Input } from "../../AKit/Input/input";
import { Spinner } from "../../AKit/Spinner/spinner";
import { Button } from "../../AKit/Button/button";


interface LoginProps { }

export const Login: FunctionComponent<LoginProps> = observer(() => {
  const [name, setName] = useState("");
  const [room, setRoom] = useState("");

  const [isLoading, setIsLoading] = useState(false);

  const onLoginButtonClick = (): void => {
    if (name && room) {
      setIsLoading(true);
      chatStore.loginChat(name, room);
    } else {
      alert("Введи данные");
    }
  };

  const onTextInput = useCallback((value: string, property: "name" | "room") => {
    const text = value.toLocaleLowerCase().trim();
    if (property === "name") {
      setName(text);
    } else {
      setRoom(text);
    }
  }, [])

  useEffect(() => {
    if (isLoading) {
      setIsLoading(false);
    }
  }, [isLoading])

  const errorMessage = chatStore.loginStatus.isError && (
    <Text>Пользователь с таким именем уже в системе</Text>
  );

  return (
    <Space hasFillSpace={true} align="center">
      <Title children="Вход в чат" />
      {errorMessage}
      <Input
        placeholder="Введите имя"
        hasFillSpace={false}
        onChange={value => onTextInput(value, "name")}
      />
      <Input
        placeholder="Введите название комнаты"
        hasFillSpace={false}
        onChange={value => onTextInput(value, "room")}
      />
      {isLoading ? (
        <Space direction="horizontal">
          <Text>движемся в направлении коматы</Text>
          <Spinner />
        </Space>
      ) : (
        <Button text="Войти" onClick={onLoginButtonClick} />
      )}
    </Space>
  );
});
