import { observer } from "mobx-react";
import { FunctionComponent, useEffect } from "react";
import { chatStore } from "../Store/chatStore";

import { usePageReloadedExit } from "../Hook/usePageReloadedExit";
import styles from "./app.module.scss";
import { Room } from "./Room/room";
import { Login } from "./Login/login";

export const App: FunctionComponent = observer(() => {
  usePageReloadedExit();

  return (
    <div className={styles.container}>
      {chatStore.loginStatus.isSuccessful ? <Room /> : <Login />}
    </div>
  );
});
