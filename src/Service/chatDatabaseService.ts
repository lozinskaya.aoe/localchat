import { IMessage } from "../Model/message";
import { IRoom } from "../Model/room";
import { IUser } from "../Model/user";

export class ChatDatabaseService {
  private _indexedDB = window.indexedDB;

  private _onupgradeneeded = (
    openRequest: IDBOpenDBRequest,
    nameRoom: IRoom["name"]
  ): void => {
    const db = openRequest.result;

    if (db.objectStoreNames.contains(nameRoom) === false) {
      db.createObjectStore(nameRoom, { keyPath: "name" });
    }
  };

  private _addRoomAndUpdateChatVersion = (
    db: IDBDatabase,
    nameRoom: IRoom["name"]
  ): Promise<IDBDatabase> => {
    return new Promise((resolve, reject) => {
      const version = db.version;

      db.close();

      //FIXME в момент обновление версии базы данных происходит задержка в браузере, по этой причине на вход добавлен спинер
      const secondRequest = this._indexedDB.open("chat", version + 1);

      secondRequest.onupgradeneeded = () => {
        this._onupgradeneeded(secondRequest, nameRoom);
      };

      secondRequest.onsuccess = () => {
        const db = secondRequest.result;
        const load = () => {
          return db;
        };

        resolve(load());
      };
    });
  };

  private _updateUser = (
    db: IDBDatabase,
    roomName: IRoom["name"],
    userName: IUser["name"]
  ): Promise<IUser | null> => {
    return new Promise((resolve, reject) => {
      const transaction = db.transaction(roomName, "readwrite");
      const room = transaction.objectStore(roomName);
      const userRequest = room.get(userName);

      userRequest.onsuccess = () => {
        let user: IUser = userRequest.result;

        const load = () => {
          if (user) {
            if (user.isOnline) {
              return null;
            } else {
              user.isOnline = true;
              room.put(user);
              return user;
            }
          } else {
            user = { name: userName, isOnline: true, messages: [] };
            room.add(user);
            return user;
          }
        };

        resolve(load());
      };

      userRequest.onerror = function (err) {
        console.error("Произошла ошибка при обновлении пользвателя");
        reject(err);
      };
    });
  };

  public createOrInletChatRoom = (
    roomName: IRoom["name"],
    userName: IUser["name"]
  ): Promise<IUser | null> => {
    return new Promise((resolve, reject) => {
      const openRequest = this._indexedDB.open("chat");

      openRequest.onupgradeneeded = () => {
        this._onupgradeneeded(openRequest, roomName);
      };

      openRequest.onsuccess = () => {
        const db = openRequest.result;

        const load = async () => {
          if (db.objectStoreNames.contains(roomName)) {
            const user = await this._updateUser(db, roomName, userName);
            return user;
          } else {
            const newVersBD = await this._addRoomAndUpdateChatVersion(
              db,
              roomName
            );
            const user = await this._updateUser(newVersBD, roomName, userName);
            return user;
          }
        };

        resolve(load());
      };

      openRequest.onerror = function (err) {
        console.error("Произошла ошибка при создании комнаты");
        reject(err);
      };
    });
  };

  public closeChatRoom = async (
    roomName: IRoom["name"],
    userName: IUser["name"]
  ): Promise<void> => {
    return new Promise((resolve, reject) => {
      const openRequest = this._indexedDB.open("chat");

      openRequest.onsuccess = () => {
        const db = openRequest.result;

        const transaction = db.transaction(roomName, "readwrite");
        const room = transaction.objectStore(roomName);
        const userRequest = room.get(userName);

        userRequest.onsuccess = () => {
          const user: IUser = userRequest.result;
          user.isOnline = false;

          const load = () => {
            room.put(user);
            db.close();
          };

          resolve(load());
        };
      };

      openRequest.onerror = function (err) {
        console.error("Произошла ошибка при закрытии комнаты");
        reject(err);
      };
    });
  };

  public sendMessage = async (
    roomName: IRoom["name"],
    userName: IUser["name"],
    messageContent: IMessage["content"]
  ): Promise<void> => {
    return new Promise((resolve, reject) => {
      const openRequest = this._indexedDB.open("chat");

      openRequest.onsuccess = () => {
        const db = openRequest.result;

        const transaction = db.transaction(roomName, "readwrite");
        const room = transaction.objectStore(roomName);
        const userRequest = room.get(userName);

        userRequest.onsuccess = () => {
          const user: IUser = userRequest.result;
          const message: IMessage = {
            userName: userName,
            id: Math.random().toFixed(6),
            content: messageContent,
            date: new Date().toString(),
          };
          user.messages.push(message);

          const load = () => {
            room.put(user);
            db.close();
          };

          resolve(load());
        };
      };

      openRequest.onerror = function (err) {
        console.error("Произошла ошибка при отправки сообщения");
        reject(err);
      };
    });
  };

  public getChatMessages = async (
    roomName: IRoom["name"]
  ): Promise<IMessage[]> => {
    return new Promise((resolve, reject) => {
      const openRequest = this._indexedDB.open("chat");

      openRequest.onsuccess = () => {
        const db = openRequest.result;

        const transaction = db.transaction(roomName, "readwrite");
        const room = transaction.objectStore(roomName);
        const usersRequest = room.getAll();

        usersRequest.onsuccess = () => {
          const users: IUser[] = usersRequest.result;
          const messages: IMessage[] = [];

          for (let user of users) {
            messages.push(...user.messages);
          }

          const load = () => {
            db.close();
            return messages;
          };

          resolve(load());
        };

        openRequest.onerror = function (err) {
          console.error("Произошла ошибка при получении всех сообщений");
          reject(err);
        };
      };
    });
  };
}
