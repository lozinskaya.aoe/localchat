import { IMessage } from "./message";

export interface IUser {
    name: string,
    isOnline: boolean,
    messages: IMessage[]
}

