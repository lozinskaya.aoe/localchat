export interface IMessage {
    userName: string,
    id: string,
    content: IMessageContent,
    date: string,

}

export interface IMessageContent {
    text: string,
    image: ImageFile | null,
    quotedMessage: IQuotedMessage | null
}


export interface  ImageFile {
    file: File;
    imagePreviewUrl: string;
  }

  export interface IQuotedMessage {
    id: IMessage["id"],
    userName: IMessage["userName"]
  }