import { FunctionComponent, useState } from "react";

interface ButtonLoadendProps {
  onClick: (e: React.ChangeEvent<HTMLInputElement>) => void;
  children: React.ReactNode;
}

export const ButtonLoadend: FunctionComponent<ButtonLoadendProps> = ({
  onClick,
  children = "Загрузить файл",
}) => {
  return (
    <label>
      {children}

      <input
        type="file"
        accept=".jpg, .jpeg, .png"
        multiple
        onChange={onClick}
        style={{ display: "none" }}
        id="input-field"
      />
    </label>
  );
};