import { FunctionComponent } from "react";
import ReactDOM from "react-dom";
import styles from "./modal.module.scss";

interface Modal {
  children: React.ReactNode;
}

export const Modal: FunctionComponent<Modal> = ({ children }) => {
  const modal = (
    <div className={styles.mask}>
      <div className={styles.container}>{children}</div>
    </div>
  );

  return ReactDOM.createPortal(modal, document.body);
};
