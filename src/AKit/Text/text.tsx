import { FunctionComponent } from "react";

import styles from "./text.module.scss";

interface TextProps {
  children: string | number;
  fontSize?: "12px" | "16px" | "18px";
  fontWeight?: "400" | "600";
  color?: "grey" | "dark";
  textAlign?: "end" | "start";
}

export const Text: FunctionComponent<TextProps> = ({
  children,
  fontSize = "12px",
  fontWeight = "400",
  color = "dark",
  textAlign = "start",
}) => {
  const style = { fontSize, fontWeight, textAlign };
  const className = styles[`color-${color}`];
  return (
    <span style={style} className={className}>
      {children}
    </span>
  );
};
