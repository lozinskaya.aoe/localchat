import { FunctionComponent } from "react";
import styles from "./title.module.scss";

interface TitleProps {
    lavel?: 1 | 2 | 4,
    color?: "light" | "dark" ,
    children: string | number,
    className?: string
}

export const Title: FunctionComponent<TitleProps> = ({ lavel = 2, color = "dark", children, className: classNameProps}) => {
    const getTitle = (): JSX.Element => {
        const className = `${styles.title} ${styles[`color-${color}`]} ${classNameProps}`;
        switch (lavel) {
            case 1: return <h1 className={className}>{children}</h1>
            case 2: return <h2 className={className}>{children}</h2>
            case 4: return <h4 className={className}>{children}</h4>
        }
    }
    return getTitle()
}