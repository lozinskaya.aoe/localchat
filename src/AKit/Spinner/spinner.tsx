import { FunctionComponent } from "react"
import styles from "./spinner.module.scss";


export const Spinner: FunctionComponent = () => {

    return <div className={styles.container}>...</div>
}