import { FunctionComponent, ReactNode } from "react";
import styles from "./icon.module.scss";

interface IconProps {
  children: ReactNode;
}

export const Icon: FunctionComponent<IconProps> = ({ children }) => {
  return <i className={styles.container}>{children}</i>;
};
