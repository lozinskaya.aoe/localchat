import { FunctionComponent, ReactNode } from "react";
import { Input, InputProps } from "../input";
import styles from "./inputWithIcon.module.scss";

interface InputWithProps extends InputProps {
  startContent?: ReactNode;
  endContent?:  ReactNode;
  hasFillSpace?: boolean;
}

export const InputWithIcon: FunctionComponent<InputWithProps> = ({
  startContent,
  hasFillSpace = true,
  endContent,
  ...props
}) => {
  const className = `${styles.container} ${
    hasFillSpace ? styles["fill-space"] : ""
  }`;

  return (
    <div className={className}>
      <span>{startContent}</span>
      <Input
        onChange={props.onChange}
        placeholder={props.placeholder}
        type={props.type}
        value={props.value}
      />
      <span>{endContent}</span>
    </div>
  );
};
