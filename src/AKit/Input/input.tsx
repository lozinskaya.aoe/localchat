import { FunctionComponent, useState } from "react";
import styles from "./input.module.scss";

export interface InputProps {
  type?: "text";
  placeholder?: string;
  onChange?: (value: string) => void;
  hasFillSpace?: boolean;
  className?: string;
  value?: string | number;
}

export const Input: FunctionComponent<InputProps> = ({
  type = "text",
  placeholder = "Введите текст",
  hasFillSpace = true,
  onChange: onChangeProps,
  className: classNameProps = "",
  value,
}) => {
  const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const content = event.target.value;
    onChangeProps?.(content);
  };

  const className = `${styles.input} ${
    hasFillSpace ? styles["fill-space"] : ""
  } ${classNameProps}`;

  return (
    <input
      type={type}
      placeholder={placeholder}
      onChange={onChange}
      className={className}
      maxLength={75}
      value={value}
    />
  );
};
