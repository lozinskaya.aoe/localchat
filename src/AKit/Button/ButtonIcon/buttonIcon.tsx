import { FunctionComponent } from "react";
import styles from "./buttonIcon.module.scss";


interface ButtonIconProps {
    children: React.ReactNode,
    onClick?: (e: any) => void,
    color?: "light" | "dark",
}

export const ButtonIcon: FunctionComponent<ButtonIconProps> = ({children, onClick, color="dark"}) => {
    const className = `${styles.button} ${styles[`color-${color}`]}`
    return <button className={className} onClick={onClick}>{children}</button>
}