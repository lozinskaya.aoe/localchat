import { FunctionComponent } from "react";
import styles from "./button.module.scss";


interface ButtonProps {
    text: string,
    onClick?: () => void
}

export const Button: FunctionComponent<ButtonProps> = ({text, onClick}) => {
    return <button className={styles.button} onClick={onClick}>{text}</button>
}