import { FunctionComponent, useCallback } from "react";
import styles from "./space.module.scss";

interface SpaceProps {
  gap?: number;
  align?: "center" | "left" | "between" | "right";
  justify?: "center" | "start";
  direction?: "vertical" | "horizontal";
  children: React.ReactNode;
  hasFillSpace?: boolean;
  className?: string;
  onClick?: () => void;
  onDoubleClick?: () => void;
}

export const Space: FunctionComponent<SpaceProps> = ({
  gap = 10,
  align = "center",
  justify = "center",
  direction = "vertical",
  hasFillSpace = false,
  children,
  className,
  onClick,
  onDoubleClick,
}) => {
  const style = {
    gap: `${gap}px`,
  };

  const getClassName = () => {
    const stateClassName = styles.space;
    const alignClassName = styles[`align-${direction}-${align}`];
    const justifyClassName = styles[`align-${direction}-justify-${justify}`];
    const directionClassName = styles[`direction-${direction}`];
    const fillSpace = hasFillSpace ? styles[`space-fill-${direction}`] : "";
    return `${stateClassName} ${alignClassName} ${justifyClassName} ${directionClassName} ${fillSpace} ${className}`;
  };

  return (
    <div
      style={style}
      className={getClassName()}
      onClick={onClick}
      onDoubleClick={onDoubleClick}
    >
      {children}
    </div>
  );
};
