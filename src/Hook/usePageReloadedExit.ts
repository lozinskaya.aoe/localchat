import { useEffect } from "react";
import { chatStore } from "../Store/chatStore";

export const usePageReloadedExit = () => {
  const handleExit = (event: BeforeUnloadEvent) => {
    chatStore.exitChat();
    event.preventDefault();
  }
  
  useEffect(() => {
    window.addEventListener("beforeunload", handleExit);
    return () => window.removeEventListener("beforeunload",handleExit);
  }, []);
};