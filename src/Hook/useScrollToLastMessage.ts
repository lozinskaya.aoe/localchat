import { useEffect } from "react";
import { IMessage } from "../Model/message";

export const useScrollToLastMessage = (elementRef : React.RefObject<HTMLDivElement>, messages: IMessage[]) => {
    useEffect(() => {
        if (elementRef.current) {
          const element = elementRef.current;
          if (element) {
            element.scrollTop = element.scrollHeight;
          }
        }
      }, [messages]);
};
