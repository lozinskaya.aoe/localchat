import { useEffect } from "react";
import { chatStore } from "../Store/chatStore";

export const useSyncWithBatabase = () => {
    useEffect(() => {
        const updateChat = () => {
          chatStore.updateMessageList(chatStore.roomName);
        };
        chatStore.broadcastChannel.addEventListener("message", updateChat);
        return () =>
          chatStore.broadcastChannel.removeEventListener("message", updateChat);
      }, []);
};
