import { useEffect } from "react";

export const usePressEnter = (onPress: () => void) => {
  const handleKeyDown = (e: KeyboardEvent) => {
    if (e.key === "Enter") {
      onPress();
    }
  };

  useEffect(() => {
    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, []);
};
