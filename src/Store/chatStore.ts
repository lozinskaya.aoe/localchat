import { makeAutoObservable, toJS } from "mobx";
import { IMessage, IMessageContent } from "../Model/message";
import { IRoom } from "../Model/room";
import { IUser } from "../Model/user";
import { ChatDatabaseService } from "../Service/chatDatabaseService";
import { SortDate } from "../Utils/sortDate";
import { LoginStatus } from "./type";

class ChatStore {
  private _user: IUser | null = null;
  private _roomName: IRoom["name"] = "У чата нет имени";
  private _loginStatus: LoginStatus = { isSuccessful: false, isError: false };
  private _chatDatabaseService = new ChatDatabaseService();
  private _messageList: IMessage[] = [];
  private _broadcastChannel = new BroadcastChannel("channel_identifier");

  private _isG = false;

  private _currentMessageContent: IMessageContent = {
    text: "",
    image: null,
    quotedMessage: null,
  };

  constructor() {
    makeAutoObservable(this);
  }

  public loginChat = async (
    userName: IUser["name"],
    roomName: IRoom["name"]
  ): Promise<void> => {

    const user = await this._chatDatabaseService.createOrInletChatRoom(
      roomName,
      userName
    );

    if (user) {
      this.updateMessageList(roomName);
      this._setRoomName(roomName);
      this._setUser(user);
      this._setLoginStatus({ isSuccessful: true, isError: false });
      this._broadcastChannel = new BroadcastChannel(roomName);
    } else {
      this._setLoginStatus({ isSuccessful: false, isError: true });
    }
  };

  public async updateMessageList(roomName: IRoom["name"]) {
    const messanges = await this._chatDatabaseService.getChatMessages(
      roomName
    );
    const sortList = messanges.sort((a, b) =>
      SortDate.fromString(a.date, b.date)
    );
    this._messageList = sortList;
  }

  public async exitChat(): Promise<void> {
    if (this.user)
      await this._chatDatabaseService.closeChatRoom(
        this._roomName,
        this.user.name
      );

    this._broadcastChannel.close();
    this._setUser(null);
    this._setRoomName("У чата нет имени");
    this._setLoginStatus({ isSuccessful: false, isError: false });
  }

  private _setLoginStatus(value: LoginStatus): void {
    this._loginStatus = value;
  }

  public get loginStatus(): LoginStatus {
    return this._loginStatus;
  }

  private _setUser(user: IUser | null): void {
    this._user = user;
  }

  public get user(): IUser | null {
    return this._user;
  }

  public setCurrentMessageContent<
    Q extends keyof IMessageContent,
    T extends IMessageContent[Q]
  >(property: Q, value: T): void {
    this._currentMessageContent[property] = value;
  }

  public get currentMessageContent(): IMessageContent {
    return this._currentMessageContent;
  }

  private _setRoomName(name: IRoom["name"]): void {
    this._roomName = name;
  }

  public get roomName(): IRoom["name"] {
    return this._roomName;
  }

  public async sendMessange(): Promise<void> {
    this._broadcastChannel.postMessage("message");
    if (this.user) {
      await this._chatDatabaseService.sendMessage(
        this._roomName,
        this.user.name,
        toJS(this._currentMessageContent)
      );
      await this.updateMessageList(this._roomName);
      this._cleanCurrentMessageContent();
    }
  }

  public get messangeList(): IMessage[] {
    return this._messageList;
  }

  public get broadcastChannel(): BroadcastChannel {
    return this._broadcastChannel;
  }

  public getMessgeQuoted(quoterId: IMessage["id"]): IMessage | null {
    const message = this.messangeList.find((ms) => ms.id === quoterId);
    return message ?? null;
  }

  private _cleanCurrentMessageContent(): void {
    this._currentMessageContent = {
      text: "",
      image: null,
      quotedMessage: null,
    };
  }
}

export const chatStore = new ChatStore();
