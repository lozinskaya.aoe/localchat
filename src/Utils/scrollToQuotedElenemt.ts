export const scrollToQuotedElement = async (
  elementById: string,
  style: string
) => {
  const element = document.getElementById(elementById);
  if (element) {
    element.scrollIntoView({ behavior: "smooth", block: "center" });
    const className = element.className;
    if (className.includes(style) === false) {
      element.className = `${className} ${style}`;
      return setTimeout(() => (element.className = className), 2000);
    }
  }
};
