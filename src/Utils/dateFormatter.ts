export class DateFormatter {
  static fromString = (value: string): string => {
    const formDate = new Date(value);
    const currentNow = new Date().toDateString();
    const date = formDate.toDateString();
    const time = formDate.toLocaleTimeString().slice(0, 5);
    if (currentNow === date) {
      return `${date} ${time}`;
    }
    return `${time}`;
  };
}
