export class SortDate {
  static fromString = (a: string, b: string) => {
    const dateOne = new Date(a);
    const dateTwo = new Date(b);
    if ((dateOne) > dateTwo) {
      return 1;
    }
    if (dateOne < dateTwo) {
      return -1;
    }
    return 0;
  };
}
